Texture2D Texture : register(t0);
sampler   Sampler : register(s0);

struct ps_data
{
	float2 tex[4] : TEXCOORD;
	float4 pos : SV_POSITION;
	float  fmt : PIX_FMT;
};

float4 main(ps_data _in) : SV_TARGET
{
	float4 c = float4(0,0,0,0);
	if (_in.fmt < 0.5f)
	{
		c = Texture.Sample(Sampler, _in.tex[0]);
	}
	else
	{
		float y = Texture.Sample(Sampler, _in.tex[1]).r;
		float u = Texture.Sample(Sampler, _in.tex[2]).r;
		float v = Texture.Sample(Sampler, _in.tex[3]).r;
		float4 channels = float4(y, u, v, 1.0);
		float4x4 conversion = float4x4( 
			1.000,  0.000,  1.402, -0.701,
			1.000, -0.344, -0.714,  0.529,
			1.000,  1.772,  0.000, -0.886,
			0.000,  0.000,  0.000,  0.000);
		c = mul(conversion, channels);
	}

	return c;
}
