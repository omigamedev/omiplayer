cbuffer perApplication : register(b0)
{
	float4x4 proj;
};

cbuffer perFrame : register(b1)
{
	float4x4 modelview; 
};

struct ps_data
{
	float4 col : COLOR0;
	float2 tex : TEXCOORD0;
	float4 pos : SV_POSITION;
};

struct vs_data
{
	float3 pos : POSITION;
	float3 col : COLOR0;
	float2 tex : TEXCOORD0;
};

ps_data main(vs_data _in)
{
	ps_data _out;

	float4x4 mvp = mul(proj, modelview);
	_out.pos = mul(mvp, float4(_in.pos, 1.0f));
	_out.col = float4(_in.col, 1.0f);
	_out.tex = _in.tex;

	return _out;
}
