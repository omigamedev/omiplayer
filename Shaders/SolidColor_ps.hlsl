struct ps_data
{
	float4 col : COLOR0;
	float2 tex : TEXCOORD0;
	float4 pos : SV_POSITION;
};

float4 main(ps_data _in) : SV_TARGET
{
	return _in.col;//*/ Texture.Sample(Sampler, _in.tex);
}
