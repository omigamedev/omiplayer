cbuffer perApplication : register(b0)
{
	float fmt;
};

cbuffer perFrame : register(b1)
{
	float4x4 proj;
	float4x4 modelview; 
	float4x4 texmat;
};

struct ps_data
{
	float2 tex[4] : TEXCOORD;
	float4 pos : SV_POSITION;
	float  fmt : PIX_FMT;
};

struct vs_data
{
	float3 pos : POSITION;
	float2 tex : TEXCOORD0;
};

ps_data main(vs_data _in)
{
	ps_data _out;

	float4x4 mvp = mul(proj, modelview);
	_out.pos = mul(mvp, float4(_in.pos, 1.0f));

	float4 coord = mul(texmat, float4(_in.tex, 0, 1));
	float y = (coord.y / 3.0f) * 2.0f;
	float y2 = (coord.y + 2) / 3.0f;
	_out.tex[0] = float2(coord.xy);
	_out.tex[1] = float2(coord.x, y);
	_out.tex[2] = float2(coord.x * 0.5, y2);
	_out.tex[3] = float2(coord.x * 0.5 + 0.5, y2);

	_out.fmt = fmt;

	return _out;
}
