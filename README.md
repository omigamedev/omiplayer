![omiPlayer-banner.png](https://bitbucket.org/repo/XKdKdL/images/2129969955-omiPlayer-banner.png)

# omiPlayer #

Media player for Oculus Rift and HTC Vive. It can playback mp4 (h.264 with yuv420p) and png/jpg. The **Auto-reload** feature let you save some clicks when rendering :)

![open-with-omiplayer.png](https://bitbucket.org/repo/XKdKdL/images/4285692939-open-with-omiplayer.png)

## Installation ##

You can simply grab the code and compile it using **Visual Studio 2015**. You will need the **WIX TOOLSET** (http://wixtoolset.org) to create and installer, but it's optional.

Alternatively you can just go to the **Downloads** section and install from the provided setup.

## Features ##
This player is designed to be highly multithreaded and it's able to reproduce 4K videos at 60fps smoothly while keeping the rendering at a constant 75Hz update rate. It uses LibOVR 0.4.4 and will be soon updated to the 0.6.0 when it will be released.

* **Shell integration**: just right click on a video or image and open with omiPlayer

* **DirectMode**: for the best experience the player supports the Oculus DirectMode that offers the best performace.

* **Mirror**: a window mirrors what it's in the Oculus so others can see what we are looking at.

* **Auto-reload**: this feature works only with images and it's awesome for the artists that can render a frame and instantly get it into the HMD.

## Video ##

Video demonstration: [https://www.youtube.com/watch?v=jHW5Xewj65A](https://www.youtube.com/watch?v=jHW5Xewj65A)

![omiplayer-yt.PNG](https://bitbucket.org/repo/XKdKdL/images/345359471-omiplayer-yt.PNG)

## Screenshots ##

![player.PNG](https://bitbucket.org/repo/XKdKdL/images/2765498358-player.PNG)