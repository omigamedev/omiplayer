@echo off
set SOLUTION=%1
set DEST=%2
set PLATFORM=%3
set ARCH=%4

xcopy "Libs\openvr\bin\win32\openvr_api.dll" %DEST% /D /y

cd "Libs\ffmpeg-3.0-win%ARCH%-dev\bin"
for /r %%f in (*.dll) do @xcopy "%%f" %2 /D /y

rem cd "Libs\libav\win%ARCH%\usr\bin"
rem for /r %%f in (*.dll) do @xcopy "%%f" %2 /D /y

cd %SOLUTION%
