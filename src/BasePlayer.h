#pragma once

#include "Decoder.h"
#include "DX.h"

namespace omi
{
	class BasePlayer
	{
		using This = BasePlayer;
		Format file;
		Timer timer;
		ALCdevice*  oalDevice = nullptr;
		ALCcontext* oalContext = nullptr;
		ALuint audio_source = 0;
		Texture2D video_tex[2];
		uint8_t* video_data = nullptr;
		std::atomic<bool> video_ready = false;
		int video_index = 0;
		//VideoDecoder::VideoFrame last_vframe;
		ID3D11Device* d3d_device;
		ID3D11DeviceContext* d3d_ctx;
		std::deque<ALuint> audio_buffer_queue;
		std::thread vthread;
		std::thread athread;
		volatile bool running = false;
		volatile bool playing = false;
		std::condition_variable play_cv;
		mutable std::mutex mutex;
	public:
		int video_w = 0;
		int video_h = 0;
		template<typename FrameType>
		bool Sync(const FrameType& frame)
		{
			if (!frame.valid) return false; //todo: how about using an enum??
	
			// Sync with PTS
			auto dt = frame.pts - timer.Get(); //todo: limit max to frame period
			while (running && dt >= 0)
			{
				if (!playing)
				{
					// Wait for the play signal
					std::unique_lock<std::mutex> lock(mutex);
					play_cv.wait(lock, [&]{ return !running | playing; });
				}
				std::this_thread::sleep_for(std::chrono::milliseconds((int)(dt * 1000)));
				dt = frame.pts - timer.Get();
			}
			return true;
		}
		void VideoThread();
		void AudioThread();
		// Not thread-safe
		void CleanAudioBuffer();
		bool Init(ID3D11Device* device, ID3D11DeviceContext* ctx);
		void Destroy();
		bool Open(const std::string filename);
		void Start();
		void Pause();
		void Close();
		void Toggle();
		bool UpdateTexture();
		const Texture2D& GetTexture() const;
	};
}
