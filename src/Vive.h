#pragma once

#include "Window.h"
#include "DX.h"
#include "HMD.h"

namespace omi
{
    struct Vive : public HMDInterface
    {
        uint32_t eyeWidth, eyeHeight;
        RenderTarget eyes[2];
        vr::IVRSystem* hmd;
        vr::IVRCompositor* comp;

        virtual bool Initialize() override;
        virtual bool Create(const DX& dx, const Window& window) override;
        virtual void Draw(const DX& dx, DrawFunction lambda, PostFunction postRender) override;
        virtual void Recenter() override;
        virtual int GetEyeWidth() override;
        virtual int GetEyeHeight() override;
        virtual Adapter GetAdapter(const DX& dx) override;
    };
}
