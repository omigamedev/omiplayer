#pragma once

#include "Window.h"
#include "DX.h"
#include "HMD.h"

namespace omi
{
    struct Screen : public HMDInterface
    {
        virtual bool Initialize() override;
        virtual bool Create(const DX& dx, const Window& window) override;
        virtual void Draw(const DX& dx, DrawFunction lambda, PostFunction postRender) override;
        virtual void Recenter() override;
        virtual int GetEyeWidth() override;
        virtual int GetEyeHeight() override;
        virtual Adapter GetAdapter(const DX& dx) override;
    };
}
