#include "stdafx.h"
#include "DX.h"

using namespace omi;

HRESULT res;

std::vector<Adapter> DX::EnumerateAdapters()
{
	std::vector<Adapter> list;
	IDXGIFactory* pFactory{ nullptr };
	IDXGIAdapter* pAdapter{ nullptr };
	unsigned int adapterIndex{ 0 };

	CreateDXGIFactory1(__uuidof(IDXGIFactory), reinterpret_cast<void**>(&pFactory));
	while (pFactory->EnumAdapters(adapterIndex, &pAdapter) != DXGI_ERROR_NOT_FOUND)
	{
		list.push_back(pAdapter);
		adapterIndex++;
	}
	pFactory->Release();

	return std::move(list);
}

ID3D11Device* DX::GetDevice()
{
	return d3d_device;
}

ID3D11DeviceContext* DX::GetContext()
{
	return d3d_context;
}

void DX::CreateDeviceAndSwapChain(const Adapter& adapter, const HWND handle, UINT bufferWidth, UINT bufferHeight)
{
	bufferW = bufferWidth;
	bufferH = bufferHeight;

	static DXGI_SWAP_CHAIN_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.BufferCount = 2;
	desc.BufferDesc.Width = bufferWidth;
	desc.BufferDesc.Height = bufferHeight;
	desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.BufferDesc.RefreshRate.Numerator = 0;
	desc.BufferDesc.RefreshRate.Denominator = 1;
	desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	desc.OutputWindow = handle;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	desc.Flags = 0;
	desc.Windowed = TRUE;

	auto res = D3D11CreateDeviceAndSwapChain(adapter.dx_adapter, D3D_DRIVER_TYPE_UNKNOWN, NULL, 0, nullptr, 0,
		D3D11_SDK_VERSION, &desc, &dx_swapchain, &d3d_device, nullptr, &d3d_context);
	assert(SUCCEEDED(res));
}

void DX::CreateFramebuffer()
{
	ID3D11Texture2D* colorTex{ nullptr };
	res = dx_swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&colorTex);
	assert(SUCCEEDED(res));
	framebuffer.Create(d3d_device, colorTex);
	assert(SUCCEEDED(res));
	//colorTex->Release();
}

bool DX::Create(const Adapter& adapter, const HWND handle, int width, int height)
{

	CreateDeviceAndSwapChain(adapter, handle, width, height);
	CreateFramebuffer();
	framebuffer.Use(d3d_context);

	return true;
}

void DX::Present() const
{
	dx_swapchain->Present(0, 0);
}

void DX::ClearRenderTargetView(float r, float g, float b, float a /*= 1.f*/) const
{
	framebuffer.Clear(d3d_context, r, g, b);
}

bool Adapter::operator==(const LUID& otherLuid) const
{
	return !((otherLuid.HighPart ^ luid.HighPart) | (otherLuid.LowPart ^ luid.LowPart));
}

bool Adapter::operator==(const Adapter& other) const
{
	return !((other.luid.HighPart ^ luid.HighPart) | (other.luid.LowPart ^ luid.LowPart));
}

Adapter::Adapter(Adapter&& other)
{
	dx_adapter = other.dx_adapter;
	other.dx_adapter = nullptr;
	memory = other.memory;
	other.memory = 0;
	strcpy_s(description, other.description);
	luid = other.luid;
}

Adapter::Adapter(IDXGIAdapter* adapter)
{
	this->dx_adapter = adapter;

    if (adapter != nullptr)
    {
        DXGI_ADAPTER_DESC desc;
        adapter->GetDesc(&desc);
        memory = desc.DedicatedVideoMemory / 1024u / 1024u;
        wcstombs_s(nullptr, description, sizeof(description), desc.Description, sizeof(desc.Description));
        luid = desc.AdapterLuid;
    }
}

Adapter::~Adapter()
{
	if (dx_adapter)
		dx_adapter->Release();
}

DataBuffer::DataBuffer(DataBuffer&& other)
{
	size = other.size;
	d3d_buffer = other.d3d_buffer;
	other.d3d_buffer = nullptr;
}


bool DataBuffer::Create(ID3D11Device* d3d_device, D3D11_USAGE usage, D3D11_BIND_FLAG bind, 
	int access, int size, const void* raw_data /*= nullptr*/)
{
	this->size = size;
	// size, usage, bind, cpu_access, misc, stride
	D3D11_BUFFER_DESC desc;
	desc.ByteWidth = size;
	desc.Usage = usage;
	desc.BindFlags = bind;
	desc.CPUAccessFlags = access;
	desc.MiscFlags = 0;
	desc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA data{ raw_data, 0, 0 };
	auto res = d3d_device->CreateBuffer(&desc, raw_data ? &data : nullptr, &d3d_buffer);
	assert(SUCCEEDED(res));

	return SUCCEEDED(res);
}

DataBuffer::~DataBuffer()
{
	if (d3d_buffer)
		d3d_buffer->Release();
	d3d_buffer = nullptr;
}

void DepthBuffer::Create(ID3D11Device* device, int width, int height)
{
	static D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Width = width;
	desc.Height = height;
	desc.ArraySize = 1;
	desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	desc.CPUAccessFlags = 0;
	desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	desc.MipLevels = 1;
	desc.MiscFlags = 0;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;

	res = device->CreateTexture2D(&desc, nullptr, &texture);
	assert(SUCCEEDED(res));
	res = device->CreateDepthStencilView(texture, nullptr, &view);
	assert(SUCCEEDED(res));

	static D3D11_DEPTH_STENCIL_DESC dsd;
	ZeroMemory(&dsd, sizeof(dsd));
	dsd.DepthEnable = TRUE;
	dsd.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsd.DepthFunc = D3D11_COMPARISON_LESS;
	dsd.StencilEnable = FALSE;
	res = device->CreateDepthStencilState(&dsd, &state);
	assert(SUCCEEDED(res));
}

bool Texture2D::Create(ID3D11Device* device, int width, int height, D3D11_BIND_FLAG bind, D3D11_USAGE usage, 
	int access, DXGI_FORMAT format, int mip_levels, void* data /*= nullptr*/)
{
	w = width;
	h = height;

	static D3D11_TEXTURE2D_DESC texDesc;
	ZeroMemory(&texDesc, sizeof(texDesc));
	texDesc.Width = width;
	texDesc.Height = height;
	texDesc.ArraySize = 1;
	texDesc.BindFlags = bind | D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = access;
	texDesc.Format = format;
	texDesc.MipLevels = mip_levels;
	texDesc.MiscFlags = 0;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = usage;

	D3D11_SUBRESOURCE_DATA tsrd{ 0 };
	tsrd.pSysMem = data;
	tsrd.SysMemPitch = width * 4;

	res = device->CreateTexture2D(&texDesc, data ? &tsrd : nullptr, &texture);
	assert(SUCCEEDED(res));

	D3D11_SHADER_RESOURCE_VIEW_DESC resourceDesc;
	ZeroMemory(&resourceDesc, sizeof(resourceDesc));
	resourceDesc.Format = texDesc.Format;
	resourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	resourceDesc.Texture2D.MipLevels = mip_levels;
	res = device->CreateShaderResourceView(texture, &resourceDesc, &view);
	assert(SUCCEEDED(res));

	return SUCCEEDED(res);
}

bool Texture2D::Create(ID3D11Device* device, ID3D11Texture2D* tex, bool createShaderResource /*= true*/)
{
	D3D11_TEXTURE2D_DESC desc;

	texture = tex;
	texture->GetDesc(&desc);

	if (createShaderResource && (desc.BindFlags & D3D11_BIND_SHADER_RESOURCE))
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC resourceDesc;
		ZeroMemory(&resourceDesc, sizeof(resourceDesc));
		resourceDesc.Format = desc.Format;
		resourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		resourceDesc.Texture2D.MipLevels = desc.MipLevels;
		res = device->CreateShaderResourceView(texture, &resourceDesc, &view);
		assert(SUCCEEDED(res));
	}

	return true;
}

void Texture2D::Update(ID3D11DeviceContext* ctx, void* data)
{
	//ctx->UpdateSubresource(texture, 0, nullptr, data, w*4, 0);
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	ctx->Map(texture, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	memcpy(mappedResource.pData, data, w*h*4);
	ctx->Unmap(texture, 0);
}

bool RenderTarget::Create(ID3D11Device* device, int width, int height)
{
	w = width;
	h = height;

	color.Create(device, width, height, D3D11_BIND_RENDER_TARGET, D3D11_USAGE_DEFAULT, 0, DXGI_FORMAT_R8G8B8A8_UNORM, 1);
	depth.Create(device, width, height);

	D3D11_TEXTURE2D_DESC desc;
	color.texture->GetDesc(&desc);

	D3D11_RENDER_TARGET_VIEW_DESC renderDesc;
	ZeroMemory(&renderDesc, sizeof(renderDesc));
	renderDesc.Format = desc.Format;
	renderDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	res = device->CreateRenderTargetView(color.texture, &renderDesc, &view);
	assert(SUCCEEDED(res));

	D3D11_RASTERIZER_DESC rd;
	ZeroMemory(&rd, sizeof(rd));
	rd.AntialiasedLineEnable = FALSE;
	rd.CullMode = D3D11_CULL_NONE;
	rd.DepthBias = 0;
	rd.DepthBiasClamp = .0f;
	rd.DepthClipEnable = TRUE;
	rd.FillMode = D3D11_FILL_SOLID;
	rd.FrontCounterClockwise = FALSE;
	rd.MultisampleEnable = FALSE;
	rd.ScissorEnable = FALSE;
	rd.SlopeScaledDepthBias = .0f;
	res = device->CreateRasterizerState(&rd, &rasterizer);
	assert(SUCCEEDED(res));

	return true;
}

bool RenderTarget::Create(ID3D11Device* device, ID3D11Texture2D* tex)
{
	color.Create(device, tex);

	D3D11_TEXTURE2D_DESC desc;
	color.texture->GetDesc(&desc);

	w = desc.Width;
	h = desc.Height;

	depth.Create(device, desc.Width, desc.Height);

	D3D11_RENDER_TARGET_VIEW_DESC renderDesc;
	ZeroMemory(&renderDesc, sizeof(renderDesc));
	renderDesc.Format = desc.Format;
	renderDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	res = device->CreateRenderTargetView(color.texture, &renderDesc, &view);
	assert(SUCCEEDED(res));

	D3D11_RASTERIZER_DESC rd;
	ZeroMemory(&rd, sizeof(rd));
	rd.AntialiasedLineEnable = FALSE;
	rd.CullMode = D3D11_CULL_NONE;
	rd.DepthBias = 0;
	rd.DepthBiasClamp = .0f;
	rd.DepthClipEnable = TRUE;
	rd.FillMode = D3D11_FILL_SOLID;
	rd.FrontCounterClockwise = FALSE;
	rd.MultisampleEnable = FALSE;
	rd.ScissorEnable = FALSE;
	rd.SlopeScaledDepthBias = .0f;
	res = device->CreateRasterizerState(&rd, &rasterizer);
	assert(SUCCEEDED(res));

	return true;
}

void RenderTarget::Clear(ID3D11DeviceContext* ctx, float r, float g, float b, float a /*= 1.f*/)
{
	float c[4] = {r, g, b, a};
	ctx->ClearRenderTargetView(view, c);
	ctx->ClearDepthStencilView(depth.view, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1, 0);
}

void RenderTarget::Use(ID3D11DeviceContext* ctx)
{
	D3D11_VIEWPORT vp_screen{ 0.f, 0.f, (float)w, (float)h, 0.f, 1.f };
	ctx->RSSetState(rasterizer);
	ctx->RSSetViewports(1, &vp_screen);
	ctx->OMSetRenderTargets(1, &view, depth.view);
	ctx->OMSetDepthStencilState(depth.state, 0);
}

bool MultiRenderTarget::Create(ID3D11Device* device, int width, int height)
{
	w = width;
	h = height;

	depth.Create(device, width, height);

	D3D11_RASTERIZER_DESC rd;
	ZeroMemory(&rd, sizeof(rd));
	rd.AntialiasedLineEnable = FALSE;
	rd.CullMode = D3D11_CULL_NONE;
	rd.DepthBias = 0;
	rd.DepthBiasClamp = .0f;
	rd.DepthClipEnable = TRUE;
	rd.FillMode = D3D11_FILL_SOLID;
	rd.FrontCounterClockwise = FALSE;
	rd.MultisampleEnable = FALSE;
	rd.ScissorEnable = FALSE;
	rd.SlopeScaledDepthBias = .0f;
	res = device->CreateRasterizerState(&rd, &rasterizer);
	assert(SUCCEEDED(res));

	return true;
}

bool MultiRenderTarget::AddTarget(ID3D11Device* device, ID3D11Texture2D* tex /*= nullptr*/)
{
	Target target;
	DXGI_FORMAT format;

	if (tex == nullptr)
	{
		format = DXGI_FORMAT_R8G8B8A8_UNORM;
		target.color.Create(device, w, h, D3D11_BIND_RENDER_TARGET, D3D11_USAGE_DEFAULT, 0, format, 1);
	}
	else
	{
		target.color.Create(device, tex);
		D3D11_TEXTURE2D_DESC desc;
		tex->GetDesc(&desc);
		format = desc.Format;

		//TODO: match if the size matches with the depth buffer
	}

	D3D11_RENDER_TARGET_VIEW_DESC renderDesc;
	ZeroMemory(&renderDesc, sizeof(renderDesc));
	renderDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
	renderDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	res = device->CreateRenderTargetView(target.color.texture, &renderDesc, &target.view);
	assert(SUCCEEDED(res));

	targets.push_back(target);

	return true;
}

void MultiRenderTarget::Clear(ID3D11DeviceContext* ctx, float r, float g, float b, float a /*= 1.f*/)
{
	float c[4] = { r, g, b, a };
	ctx->ClearRenderTargetView(targets[currentIndex].view, c);
	ctx->ClearDepthStencilView(depth.view, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1, 0);
}

void MultiRenderTarget::Use(ID3D11DeviceContext* ctx)
{
	D3D11_VIEWPORT vp_screen{ 0.f, 0.f, (float)w, (float)h, 0.f, 1.f };
	ctx->RSSetState(rasterizer);
//	ctx->RSSetViewports(1, &vp_screen);
	ctx->OMSetRenderTargets(1, &targets[currentIndex].view, depth.view);
	ctx->OMSetDepthStencilState(depth.state, 0);
}
