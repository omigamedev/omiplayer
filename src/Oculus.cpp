#include "stdafx.h"
#include "Oculus.h"

using namespace omi;

bool OculusFramebuffer::Create(ID3D11Device* device, ovrSession session, int width, int height)
{
    ovrTextureSwapChainDesc descr;
    descr.Width = width;
    descr.Height = height;
    descr.MipLevels = 1;
    descr.ArraySize = 1;
    descr.Format = ovrTextureFormat::OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
    descr.SampleCount = 1;
    descr.MiscFlags = ovrTextureMisc_None;
    descr.BindFlags = ovrTextureBind_DX_RenderTarget;
    descr.StaticImage = ovrFalse;
    descr.Type = ovrTextureType::ovrTexture_2D;
    auto result = ovr_CreateTextureSwapChainDX(session, device, &descr, &TextureSet);
	
    if (!OVR_SUCCESS(result))
		return false;

	MultiRenderTarget::Create(device, width, height);

    int count = 0;
    ovr_GetTextureSwapChainLength(session, TextureSet, &count);

	for (int i = 0; i < count; ++i)
	{
        ID3D11Texture2D* texture = nullptr;
        ovr_GetTextureSwapChainBufferDX(session, TextureSet, i, IID_PPV_ARGS(&texture));
		AddTarget(device, texture);
	}

	return true;
}

void OculusFramebuffer::SetViewport(ID3D11DeviceContext* ctx)
{
	D3D11_VIEWPORT D3Dvp;
	D3Dvp.Width = (float)eyeRenderViewport.Size.w;
	D3Dvp.Height = (float)eyeRenderViewport.Size.h;
	D3Dvp.MinDepth = 0.f;
	D3Dvp.MaxDepth = 1.f;
	D3Dvp.TopLeftX = (float)eyeRenderViewport.Pos.x;
	D3Dvp.TopLeftY = (float)eyeRenderViewport.Pos.y;
	ctx->RSSetViewports(1, &D3Dvp);
}

void OculusFramebuffer::Commit(ovrSession session)
{
    ovr_CommitTextureSwapChain(session, TextureSet);
    ovr_GetTextureSwapChainCurrentIndex(session, TextureSet, &currentIndex);
}

bool OculusRift::Initialize()
{
    ovrResult result;
    result = ovr_Initialize(nullptr);
    if (result != ovrSuccess) return false;

    result = ovr_Create(&session, &luid);
    if (result != ovrSuccess) return false;

	hmdDesc = ovr_GetHmdDesc(session);

    // FloorLevel will give tracking poses where the floor height is 0
    ovr_SetTrackingOriginType(session, ovrTrackingOrigin_FloorLevel);

	// Make the eye render buffers (caution if actual size < requested due to HW limits). 
	for (int eye = 0; eye < 2; ++eye)
	{
		eyes[eye].idealSize = ovr_GetFovTextureSize(session, (ovrEyeType)eye, hmdDesc.DefaultEyeFov[eye], 1.0f);
		eyes[eye].eyeRenderViewport.Pos.x = 0;
		eyes[eye].eyeRenderViewport.Pos.y = 0;
		eyes[eye].eyeRenderViewport.Size = eyes[eye].idealSize;
	}

	return true;
}

bool OculusRift::Create(const DX& dx, const Window& window)
{
	for (int eye = 0; eye < 2; ++eye)
	{
		eyes[eye].Create(dx.d3d_device, session, eyes[eye].idealSize.w, eyes[eye].idealSize.h);

		// Setup VR components, filling out description
		eyeRenderDesc[eye] = ovr_GetRenderDesc(session, (ovrEyeType)eye, hmdDesc.DefaultEyeFov[eye]);
		HmdToEyeViewOffset[eye] = eyeRenderDesc[eye].HmdToEyeOffset;
	}

    // Create a mirror to see on the monitor.
    ovrMirrorTextureDesc mirrorDesc = {};
    mirrorDesc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
    mirrorDesc.Width = window.w;
    mirrorDesc.Height = window.h;
    auto result = ovr_CreateMirrorTextureDX(session, dx.d3d_device, &mirrorDesc, &mirrorTexture);

	return true;
}

void OculusRift::Draw(const DX& dx, DrawFunction lambda, PostFunction postRender)
{
	// Get both eye poses simultaneously, with IPD offset already included. 
	auto ftiming = ovr_GetPredictedDisplayTime(session, 0);
	ovrTrackingState hmdState = ovr_GetTrackingState(session, ftiming, true);
	ovr_CalcEyePoses(hmdState.HeadPose.ThePose, HmdToEyeViewOffset, EyeRenderPose);

	if (visible)
	{
		for (int eye = 0; eye < 2; ++eye)
		{
			eyes[eye].Use(dx.d3d_context);
			eyes[eye].Clear(dx.d3d_context, .3f, .3f, .3f);

			eyes[eye].SetViewport(dx.d3d_context);

			// Get view and projection matrices
            ovrMatrix4f p = ovrMatrix4f_Projection(eyeRenderDesc[eye].Fov, 0.2f, 1000.0f, ovrProjection_None);
            DirectX::XMVECTOR q = DirectX::XMVectorSet(EyeRenderPose[eye].Orientation.x, EyeRenderPose[eye].Orientation.y,
                EyeRenderPose[eye].Orientation.z, EyeRenderPose[eye].Orientation.w);

            DirectX::XMMATRIX eyeProj = DirectX::XMMatrixSet(
                p.M[0][0], p.M[1][0], p.M[2][0], p.M[3][0],
                p.M[0][1], p.M[1][1], p.M[2][1], p.M[3][1],
                p.M[0][2], p.M[1][2], p.M[2][2], p.M[3][2],
                p.M[0][3], p.M[1][3], p.M[2][3], p.M[3][3]);

			DirectX::XMMATRIX modelview = DirectX::XMMatrixRotationQuaternion(q);
            modelview = DirectX::XMMatrixTranspose(modelview);
			//DirectX::XMMATRIX offset = DirectX::XMMatrixTranslation(EyeRenderPose[eye].Position.x, EyeRenderPose[eye].Position.y, EyeRenderPose[eye].Position.z);

			lambda(eye, eyeProj, modelview);

			eyes[eye].Commit(session);
		}
		postRender();
	}

	// Initialize our single full screen Fov layer.
	ovrLayerEyeFov ld;
	ld.Header.Type = ovrLayerType_EyeFov;
	ld.Header.Flags = 0;

	for (int eye = 0; eye < 2; ++eye)
	{
		ld.ColorTexture[eye] = eyes[eye].TextureSet;
		ld.Viewport[eye] = eyes[eye].eyeRenderViewport;
		ld.Fov[eye] = hmdDesc.DefaultEyeFov[eye];
		ld.RenderPose[eye] = EyeRenderPose[eye];
	}

	ovrLayerHeader* layers = &ld.Header;
	auto result = ovr_SubmitFrame(session, 0, nullptr, &layers, 1);
	// exit the rendering loop if submit returns an error, will retry on ovrError_DisplayLost
	assert(OVR_SUCCESS(result));

	visible = (result == ovrSuccess);

	// Render mirror
    ID3D11Texture2D* tex = nullptr;
    ovr_GetMirrorTextureBufferDX(session, mirrorTexture, IID_PPV_ARGS(&tex));
	dx.d3d_context->CopyResource(dx.framebuffer.color.texture, tex);
	dx.Present();
}

void OculusRift::Recenter()
{
	ovr_RecenterTrackingOrigin(session);
}

Adapter OculusRift::GetAdapter(const DX& dx)
{
    LUID l = *reinterpret_cast<const LUID*>(&luid);
    auto adapters = DX::EnumerateAdapters();
    auto filter = [l](const Adapter& x){ return x == l; };
    auto selected = std::find_if(adapters.begin(), adapters.end(), filter);
    if (selected == adapters.end())
    	return Adapter(nullptr);
    return std::move(*selected);
}
