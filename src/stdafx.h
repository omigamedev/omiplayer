#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <assert.h>
#include <windows.h>
#include <Shlwapi.h> // for PathRemoveFileSpec
#include <sys/stat.h>

#include <map>
#include <deque>
#include <memory>
#include <vector>
#include <thread>
#include <atomic>
#include <mutex>
#include <regex>
#include <fstream>
#include <algorithm>
#include <functional>
#include <condition_variable>

#include <d3d11.h>
#include <dxgi1_3.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <DirectXColors.h>

#include <OVR_CAPI.h>
#include <OVR_CAPI_D3D.h>
#include <OVR_CAPI_Audio.h>

#include <openvr.h>

#include "stb.h"
#include "stb_image.h"

#include <al.h>
#include <alc.h>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>
}
