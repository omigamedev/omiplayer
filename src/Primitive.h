#pragma once

#include "../Shaders/SolidColor_ps.h"
#include "../Shaders/SolidColor_vs.h"
#include "../Shaders/Textured_ps.h"
#include "../Shaders/Textured_vs.h"

namespace omi
{
	using namespace DirectX;

	struct IBaseMesh
	{
		DataBuffer vertexBuffer;
		DataBuffer indexBuffer;
		int indexCount;

		virtual void Draw(ID3D11DeviceContext* ctx) = 0;
	};

	__declspec(align(16)) struct Vertex
	{
		static D3D11_INPUT_ELEMENT_DESC Layout[];

		XMFLOAT3 Position;
		XMFLOAT2 Tex;
		Vertex() = default;
		Vertex(XMFLOAT3 p, XMFLOAT2 t) : Position(p), Tex(t) { }
		Vertex(XMFLOAT3 p) : Position(p) { }
	};
	D3D11_INPUT_ELEMENT_DESC Vertex::Layout[]
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Vertex, Position), D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, offsetof(Vertex, Tex)     , D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	struct ShaderData
	{
		__declspec(align(16)) struct PerFrame
		{
			DirectX::XMMATRIX proj;
			DirectX::XMMATRIX modelview;
			DirectX::XMMATRIX texmat;
		} pf;

		__declspec(align(16)) struct PerApp
		{
			float fmt;
		} pa;
	};

	struct SolidColorShader : Shader<ShaderData, Vertex>
	{
		ShaderData data;

		bool Create(ID3D11Device* device)
		{
			return Shader::Create(device, SolidColorVS, SolidColorPS);
		}

		void UpdateData(ID3D11DeviceContext* ctx)
		{
			UpdatePerAppData(ctx, data.pa);
			UpdatePerFrameData(ctx, data.pf);
		}
	};

	struct TexturedShader : Shader<ShaderData, Vertex>
	{
		ID3D11SamplerState* sampler;
		ID3D11ShaderResourceView* texture;
		ShaderData data;

		bool Create(ID3D11Device* device, ID3D11ShaderResourceView* tex)
		{
			HRESULT res;

			texture = tex;

			Shader::Create(device, TexturedVS, TexturedPS);

			D3D11_SAMPLER_DESC sdesc;
			ZeroMemory(&sdesc, sizeof(sdesc));
			sdesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
			sdesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
			sdesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
			sdesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
			sdesc.MipLODBias = 0.0f;
			sdesc.MaxAnisotropy = 1;
			sdesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
			sdesc.BorderColor[0] = 0.0f;
			sdesc.BorderColor[1] = 0.0f;
			sdesc.BorderColor[2] = 0.0f;
			sdesc.BorderColor[3] = 0.0f;
			sdesc.MinLOD = -FLT_MAX;
			sdesc.MaxLOD = FLT_MAX;
			res = device->CreateSamplerState(&sdesc, &sampler);
			assert(SUCCEEDED(res));

			return true;
		}

		void UpdateData(ID3D11DeviceContext* ctx)
		{
			UpdatePerAppData(ctx, data.pa);
			UpdatePerFrameData(ctx, data.pf);
		}

		void Use(ID3D11DeviceContext* ctx)
		{
			Shader::Use(ctx);
			ctx->PSSetSamplers(0, 1, &sampler);
			ctx->PSSetShaderResources(0, 1, &texture);
		}

		void SetTexture(ID3D11DeviceContext* ctx, ID3D11ShaderResourceView* tex)
		{
			texture = tex;
			ctx->PSSetShaderResources(0, 1, &texture);
		}
	};

	struct PlaneMesh : public IBaseMesh
	{
		bool Create(ID3D11Device* device, float width, float height)
		{
			static std::vector<Vertex> rectVertices
			{
                { XMFLOAT3(-1,-.5f,0), XMFLOAT2(0,1) }, // 0
                { XMFLOAT3(-1, .5f,0), XMFLOAT2(0,0) }, // 1
                { XMFLOAT3(1, .5f,0),  XMFLOAT2(1,0) }, // 2
                { XMFLOAT3(1,-.5f,0),  XMFLOAT2(1,1) }, // 3
			};
			static std::vector<WORD> rectIndices{ 0, 1, 2, 3, 0, 2 };
			vertexBuffer.Create(device, D3D11_USAGE_DEFAULT, D3D11_BIND_VERTEX_BUFFER, 0, rectVertices);
			indexBuffer.Create(device, D3D11_USAGE_DEFAULT, D3D11_BIND_INDEX_BUFFER, 0, rectIndices);
			indexCount = rectIndices.size();
			return true;
		}

		virtual void Draw(ID3D11DeviceContext* ctx) override
		{
			UINT stride = sizeof(Vertex), offset = 0;
			ctx->IASetIndexBuffer(indexBuffer.d3d_buffer, DXGI_FORMAT_R16_UINT, 0);
			ctx->IASetVertexBuffers(0, 1, &vertexBuffer.d3d_buffer, &stride, &offset);
			ctx->DrawIndexed(indexCount, 0, 0);
		}
	};

	struct SphereMesh : public IBaseMesh
	{
		bool Create(ID3D11Device* device, float radius, int rings, int sectors)
		{
			std::vector<Vertex> sphereVertices;
			std::vector<WORD> sphereIndices;
			float phiStep = XM_PI / rings;
			float thetaStep = XM_2PI / sectors;

			for (int i = 1; i <= rings - 1; i++)
			{
				float phi = i * phiStep;
				for (int j = 0; j <= sectors; j++) 
				{
					float theta = j * thetaStep;
			
					float x = radius * std::sinf(phi) * cosf(theta);
					float y = radius * std::cosf(phi);
					float z = radius * std::sinf(phi) * sinf(theta);
					float u = theta / XM_2PI;
					float v = phi / XM_PI;
					sphereVertices.emplace_back(XMFLOAT3(x, y, z), XMFLOAT2(u, v));
				}
			}

			for (int i = 1; i <= sectors; i++) 
			{
				sphereIndices.push_back(0);
				sphereIndices.push_back(i + 1);
				sphereIndices.push_back(i);
			}
			int baseIndex = 0;
			int ringVertexCount = sectors + 1;
			for (int i = 0; i < rings - 2; i++) 
			{
				for (int j = 0; j < sectors; j++) 
				{
					sphereIndices.push_back(baseIndex + i*ringVertexCount + j);
					sphereIndices.push_back(baseIndex + i*ringVertexCount + j + 1);
					sphereIndices.push_back(baseIndex + (i + 1)*ringVertexCount + j);

					sphereIndices.push_back(baseIndex + (i + 1)*ringVertexCount + j);
					sphereIndices.push_back(baseIndex + i*ringVertexCount + j + 1);
					sphereIndices.push_back(baseIndex + (i + 1)*ringVertexCount + j + 1);
				}
			}
			int southPoleIndex = sphereVertices.size() - 1;
			baseIndex = southPoleIndex - ringVertexCount - 1;
			for (int i = 0; i < sectors; i++) 
			{
				sphereIndices.push_back(southPoleIndex);
				sphereIndices.push_back(baseIndex + i);
				sphereIndices.push_back(baseIndex + i + 1);
			}

			vertexBuffer.Create(device, D3D11_USAGE_DEFAULT, D3D11_BIND_VERTEX_BUFFER, 0, sphereVertices);
			indexBuffer.Create(device, D3D11_USAGE_DEFAULT, D3D11_BIND_INDEX_BUFFER, 0, sphereIndices);
			indexCount = sphereIndices.size();

			return true;
		}

		virtual void Draw(ID3D11DeviceContext* ctx) override
		{
			UINT stride = sizeof(Vertex), offset = 0;
			ctx->IASetIndexBuffer(indexBuffer.d3d_buffer, DXGI_FORMAT_R16_UINT, 0);
			ctx->IASetVertexBuffers(0, 1, &vertexBuffer.d3d_buffer, &stride, &offset);
			ctx->DrawIndexed(indexCount, 0, 0);
		}
	};

}
