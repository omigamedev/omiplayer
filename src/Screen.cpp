#include "stdafx.h"
#include "Screen.h"

using namespace omi;

bool Screen::Initialize()
{
    return true;
}

bool Screen::Create(const DX& dx, const Window& window)
{
    return true;
}

void Screen::Draw(const DX& dx, DrawFunction lambda, PostFunction postRender)
{
    static const auto proj = DirectX::XMMatrixOrthographicLH(1, 1, -1, 1);
    static const auto mv = DirectX::XMMatrixIdentity();
    dx.UseFramebuffer();
    dx.ClearRenderTargetView(1, 0, 0);
    lambda(0, proj, mv);
    dx.Present();
    postRender();
}

void Screen::Recenter()
{

}

int Screen::GetEyeWidth()
{
    return 960;
}

int Screen::GetEyeHeight()
{
    return 1080;
}

Adapter Screen::GetAdapter(const DX& dx)
{
    auto adapters = DX::EnumerateAdapters();
    auto filter = [](const Adapter& a, const Adapter& b) { return a.memory < b.memory; };
    auto selected = std::max_element(adapters.begin(), adapters.end(), filter);
    return std::move(*selected);
}
