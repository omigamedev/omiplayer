#include "stdafx.h"
#include "Window.h"
#include "DX.h"
#include "Primitive.h"
#include "Oculus.h"
#include "Vive.h"
#include "Screen.h"
#include "BasePlayer.h"

#define DBG

void RegisterShellExtension()
{
    static const char reg_path[] = "SOFTWARE\\Classes\\*\\shell\\Open with omiPlayer";
    static const char reg_cmd[] = "SOFTWARE\\Classes\\*\\shell\\Open with omiPlayer\\command";
    static char exe[256];
    static char str[256];

    HKEY hKey;
    HKEY hKey_cmd;
    LONG res;

    res = RegOpenKeyExA(HKEY_CURRENT_USER, reg_path, 0, KEY_WRITE, &hKey);
    if (res == ERROR_SUCCESS)
    {
        DBG("key found");
    }
    else
    {
        DBG("key not found");
        DWORD dwDisposition;
        res = RegCreateKeyExA(HKEY_CURRENT_USER, reg_path, 0, nullptr, 0, KEY_WRITE, nullptr, &hKey, &dwDisposition);
        if (res != ERROR_SUCCESS)
        {
            DBG("failed to create key");
            return;
        }
    }

    res = RegOpenKeyExA(HKEY_CURRENT_USER, reg_cmd, 0, KEY_WRITE, &hKey_cmd);
    if (res == ERROR_SUCCESS)
    {
        DBG("cmd key found");
    }
    else
    {
        DBG("cmd key not found");
        DWORD dwDisposition;
        res = RegCreateKeyExA(HKEY_CURRENT_USER, reg_cmd, 0, nullptr, 0, KEY_WRITE, nullptr, &hKey_cmd, &dwDisposition);
        if (res != ERROR_SUCCESS)
        {
            DBG("failed to create command key");
            RegCloseKey(hKey);
            return;
        }
    }

    GetModuleFileNameA(0, exe, sizeof(exe));

    sprintf_s(str, "Open with omiPlayer");
    if ((res = RegSetValueExA(hKey, "", 0, REG_SZ, (BYTE*)str, strlen(str))) != ERROR_SUCCESS)
        DBG("reg error writing Open with");

    sprintf_s(str, "%s,0", exe);
    if (RegSetValueExA(hKey, "Icon", 0, REG_SZ, (BYTE*)str, strlen(str)) != ERROR_SUCCESS)
        DBG("reg error writing Icon");

    sprintf_s(str, "%s \"%%1\"", exe);
    if (RegSetValueExA(hKey_cmd, "", 0, REG_SZ, (BYTE*)str, strlen(str)) != ERROR_SUCCESS)
        DBG("reg error writing command");

    RegCloseKey(hKey);
    RegCloseKey(hKey_cmd);
}


int main(int argc, char* argv[])
{
	omi::DX dx;
	omi::Window window;
	omi::TexturedShader shaderTex;
	omi::SolidColorShader shaderSolid;
	omi::PlaneMesh plane;
	omi::SphereMesh sphere;
	omi::BasePlayer player;

    RegisterShellExtension();

	omi::HMDInterface* hmd = nullptr;

    hmd = new omi::OculusRift();
    if (!hmd->Initialize())
    {
        printf("Failed to initialize OculusVR\n");
        delete hmd;
        hmd = new omi::Vive();
        if (!hmd->Initialize())
        {
            printf("Failed to initialize OpenVR\n");
            delete hmd;
            hmd = new omi::Screen();
            if (!hmd->Initialize())
            {
                printf("Failed to initialize Screen\n");
                delete hmd;
                system("pause");
                exit(0);
            }
        }
    }

	window.Create(hmd->GetEyeWidth(), hmd->GetEyeHeight() / 2);
	window.keyDown = [&](int key)
	{
		switch (key)
		{
		case VK_SPACE:
			player.Toggle();
			break;
		case VK_ESCAPE:
			PostQuitMessage(0);
			break;
		case 'R':
			hmd->Recenter();
			break;
		}
	};

	auto adapter = hmd->GetAdapter(dx);
    dx.Create(adapter, window.handle, window.w, window.h);

	hmd->Create(dx, window);

	static omi::Texture2D image;
	bool pollingEnabled = true;
	std::thread pollThread;
	bool reloadImage = false;
	uint8_t* image_data{ nullptr };
	enum class SourceType { IMAGE, VIDEO } src_type;
	enum class StereoType { NONE, TB, RL } stereo_type;
	if (argc > 1)
	{
		printf("Opening %s\n", argv[1]);
        std::cmatch matches;
        if (std::regex_search(argv[1], matches, std::regex{ R"|(^(\w:\\.*\\)([^\\\.]+)\.(\w+)$)|" }))
        {
            auto dir = matches[1].str();
            auto name = matches[2].str();
            auto ext = matches[3].str();
            
		    if (ext == "png" || ext == "jpg")
		    {
			    // Load test image
			    int image_w, image_h, image_comp;
			    image_data = stbi_load(argv[1], &image_w, &image_h, &image_comp, 4);
			    assert(image_data);
			    image.Create(dx.d3d_device, image_w, image_h, D3D11_BIND_SHADER_RESOURCE, D3D11_USAGE_DYNAMIC, 
				    D3D11_CPU_ACCESS_WRITE, DXGI_FORMAT_R8G8B8A8_UNORM, 1, image_data);
			    delete image_data;
			    image_data = nullptr;
			    src_type = SourceType::IMAGE;

			    pollThread = std::thread([&]
			    {
				    struct stat info;
				    stat(argv[1], &info);
				    while (pollingEnabled)
				    {
					    struct stat tmp_info;
					    stat(argv[1], &tmp_info);
					    if (tmp_info.st_mtime > info.st_mtime)
					    {
						    std::ifstream fs{ argv[1], std::ios::in };
						    if (fs.is_open())
						    {
							    fs.seekg(0, std::ios::end);
							    auto len = fs.tellg();
							    if (len > 128)
							    {
								    fs.close();
								    info = tmp_info;
								    image_data = stbi_load(argv[1], &image_w, &image_h, &image_comp, 4);
								    reloadImage = true;
								    printf("Image reloaded\n");
							    }
							    else
							    {
								    fs.close();
							    }
						    }
					    }
					    std::this_thread::sleep_for(std::chrono::milliseconds(100));
				    }
			    });
		    }
		    else if (ext == "mp4")
		    {
			    player.Init(dx.d3d_device, dx.d3d_context);
			    player.Open(argv[1]);
			    //player.Start();
			    src_type = SourceType::VIDEO;
		    }

		    //name[strlen(name) - strlen(ext)] = 0;
		    if (std::regex_match(name, std::regex{ ".*_RL" }))
		    {
			    stereo_type = StereoType::RL;
		    }
		    else if (std::regex_match(name, std::regex{ ".*_TB" }))
		    {
			    stereo_type = StereoType::TB;
		    }
		    else
		    {
			    switch (src_type)
			    {
			    case SourceType::IMAGE:
				    stereo_type = image.w == image.h ? StereoType::TB : StereoType::NONE;
				    break;
			    case SourceType::VIDEO:
				    stereo_type = player.video_w == player.video_h ? StereoType::TB : StereoType::NONE;
				    break;
			    }
		    }
        }
        else if (std::regex_match(argv[1], matches, std::regex{ "^https?://.*" }))
        {
            player.Init(dx.d3d_device, dx.d3d_context);
            player.Open(argv[1]);
            stereo_type = StereoType::NONE;
            src_type = SourceType::VIDEO;
            player.Start();
        }
	}

	shaderTex.Create(dx.GetDevice(), player.GetTexture().view);
	//shaderSolid.Create(dx.d3d_device);
	plane.Create(dx.GetDevice(), 1, 1);
	sphere.Create(dx.d3d_device, 1, 100, 100);

	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);
	shaderTex.Use(dx.d3d_context);

	MSG msg{ 0 };
	int frame = 0;
    auto start = std::chrono::steady_clock::now();
    float elapsed_time = 0;

	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
            auto now = std::chrono::steady_clock::now();
            std::chrono::duration<float> diff = now - start;
            start = now;
            float dt = diff.count();
            elapsed_time += dt;

            static const float ms = 1.0 / 30.0;
            if (elapsed_time < ms)
            {
                std::this_thread::sleep_for(std::chrono::duration<float>(ms - dt));
                elapsed_time -= ms;
            }


			if (reloadImage && image_data)
			{
				image.Update(dx.d3d_context, image_data);
				delete image_data;
				image_data = nullptr;
				reloadImage = false;
			}
            auto postRender = [&]()
            {
                player.UpdateTexture();
            };
			// Render stuff
			hmd->Draw(dx, [&](int eye, const DirectX::XMMATRIX& proj, const DirectX::XMMATRIX& modelview)
			{
				shaderTex.data.pf.proj = proj;
				shaderTex.data.pf.modelview = 
                    //DirectX::XMMatrixTranslation(0, 0, -1) * // translation to show the plane
                    DirectX::XMMatrixRotationY(DirectX::XMConvertToRadians(-90)) * // enable for sphere
                    modelview;
				shaderTex.data.pa.fmt = (float)src_type;

				switch (src_type)
				{
				case SourceType::IMAGE:
					shaderTex.SetTexture(dx.d3d_context, image.view);
					break;
				case SourceType::VIDEO:
					auto& t = player.GetTexture();
					shaderTex.SetTexture(dx.d3d_context, t.view);
					break;
				}

				switch (stereo_type)
				{
				case StereoType::NONE:
					shaderTex.data.pf.texmat = DirectX::XMMatrixIdentity();
					break;
				case StereoType::TB:
					shaderTex.data.pf.texmat = DirectX::XMMatrixScaling(1, .5f, 1)
						* DirectX::XMMatrixTranslation(0, (eye ? .5f : 0), 0);
					break;
				case StereoType::RL:
					shaderTex.data.pf.texmat = DirectX::XMMatrixScaling(.5f, 1, 1)
						* DirectX::XMMatrixTranslation((eye ? .5f : 0), 0, 0);
					break;
				}
				shaderTex.UpdateData(dx.d3d_context);

				//plane.Draw(dx.d3d_context);
				sphere.Draw(dx.d3d_context);
			}, postRender);
			frame++;
		}
	}
	player.Close();
	pollingEnabled = false;
	if(pollThread.joinable())
		pollThread.join();

	return 0;
}

