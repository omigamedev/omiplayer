#pragma once

#include "Window.h"
#include "DX.h"

namespace omi
{
    struct HMDInterface
    {
        typedef std::function<void(int eye, const DirectX::XMMATRIX&, const DirectX::XMMATRIX&)> DrawFunction;
        typedef std::function<void()> PostFunction;

        template<typename F, typename F2>
        void Draw(const DX& dx, F& lambda, F2& postRender)
        {
            Draw(dx, static_cast<DrawFunction>(lambda), static_cast<PostFunction>(postRender));
        }

        virtual bool Initialize() = 0;
        virtual bool Create(const DX& dx, const Window& window) = 0;
        virtual void Draw(const DX& dx, DrawFunction lambda, PostFunction postRender) = 0;
        virtual void Recenter() = 0;
        virtual Adapter GetAdapter(const DX& dx) = 0;
        virtual int GetEyeWidth() = 0;
        virtual int GetEyeHeight() = 0;
    };
}
