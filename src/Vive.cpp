#include "stdafx.h"
#include "Vive.h"

using namespace omi;

bool Vive::Create(const DX& dx, const Window& window)
{
    eyes[0].Create(dx.d3d_device, eyeWidth, eyeHeight);
    eyes[1].Create(dx.d3d_device, eyeWidth, eyeHeight);
    return true;
}

void Vive::Draw(const DX& dx, DrawFunction lambda, PostFunction postRender)
{
    vr::TrackedDevicePose_t poses[vr::k_unMaxTrackedDeviceCount];
    comp->WaitGetPoses(poses, vr::k_unMaxTrackedDeviceCount, NULL, 0);
    auto& hmdPose = poses[vr::k_unTrackedDeviceIndex_Hmd];

    //if (visible)
    {
        for (int eye = 0; eye < 2; ++eye)
        {
            eyes[eye].Use(dx.d3d_context);
            eyes[eye].Clear(dx.d3d_context, .3f, .3f, .3f);

            //eyes[eye].SetViewport(dx.d3d_context);
            D3D11_VIEWPORT D3Dvp;
            D3Dvp.Width = (float)eyes[eye].w;
            D3Dvp.Height = (float)eyes[eye].h;
            D3Dvp.MinDepth = 0.f;
            D3Dvp.MaxDepth = 1.f;
            D3Dvp.TopLeftX = (float)0;
            D3Dvp.TopLeftY = (float)0;
            dx.d3d_context->RSSetViewports(1, &D3Dvp);



            // Get view and projection matrices
            //ovrMatrix4f p = ovrMatrix4f_Projection(eyeRenderDesc[eye].Fov, 0.2f, 1000.0f, ovrProjection_None);
            vr::HmdMatrix44_t p = hmd->GetProjectionMatrix((vr::EVREye)eye, .2f, 1000.f, vr::API_OpenGL);
            DirectX::XMMATRIX eyeProj = DirectX::XMMatrixSet(
                p.m[0][0], p.m[1][0], p.m[2][0], p.m[3][0],
                p.m[0][1], p.m[1][1], p.m[2][1], p.m[3][1],
                p.m[0][2], p.m[1][2], p.m[2][2], p.m[3][2],
                p.m[0][3], p.m[1][3], p.m[2][3], p.m[3][3]);

            vr::HmdMatrix34_t e = hmd->GetEyeToHeadTransform((vr::EVREye)eye);
            DirectX::XMMATRIX eyePose = DirectX::XMMatrixSet(
                e.m[0][0], e.m[1][0], e.m[2][0], 0,
                e.m[0][1], e.m[1][1], e.m[2][1], 0,
                e.m[0][2], e.m[1][2], e.m[2][2], 0,
                e.m[0][3], e.m[1][3], e.m[2][3], 1);

            vr::HmdMatrix34_t h = hmdPose.mDeviceToAbsoluteTracking;
            DirectX::XMMATRIX hmdPose = DirectX::XMMatrixSet(
                h.m[0][0], h.m[1][0], h.m[2][0], 0,
                h.m[0][1], h.m[1][1], h.m[2][1], 0,
                h.m[0][2], h.m[1][2], h.m[2][2], 0,
                0, 0, 0, 1);

            DirectX::XMMATRIX modelview = DirectX::XMMatrixMultiply(eyePose, hmdPose);
            modelview = DirectX::XMMatrixTranspose(modelview);

            lambda(eye, eyeProj, modelview);

            vr::Texture_t eyeTexture = { (void*)eyes[eye].color.texture, vr::API_DirectX, vr::ColorSpace_Gamma };
            auto error = comp->Submit((vr::EVREye)eye, &eyeTexture);
            if (error != vr::EVRCompositorError::VRCompositorError_None)
            {
                printf("Compositor error %d\n", error);
            }
        }
        postRender();
    }
}

bool Vive::Initialize()
{
    vr::EVRInitError error;
    hmd = vr::VR_Init(&error, vr::EVRApplicationType::VRApplication_Scene);
    if (error != vr::EVRInitError::VRInitError_None) 
        return false;
    comp = vr::VRCompositor();
    if (!comp) 
        return false;

    hmd->GetRecommendedRenderTargetSize(&eyeWidth, &eyeHeight);

    return true;
}

void Vive::Recenter()
{
    
}

int Vive::GetEyeWidth()
{
    return eyeWidth;
}

int Vive::GetEyeHeight()
{
    return eyeHeight;
}

Adapter Vive::GetAdapter(const DX& dx)
{
    auto adapters = DX::EnumerateAdapters();
    
    int32_t adapterIndex;
    hmd->GetDXGIOutputInfo(&adapterIndex);

    if (adapterIndex == -1)
    {
        auto filter = [](const Adapter& a, const Adapter& b) { return a.memory < b.memory; };
        auto selected = std::max_element(adapters.begin(), adapters.end(), filter);
        return std::move(*selected);
    }
    else
    {
        return std::move(adapters[adapterIndex]);
    }
}
