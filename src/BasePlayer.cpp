#include "stdafx.h"
#include "BasePlayer.h"
#include "DX.h"

using namespace omi;

void BasePlayer::VideoThread()
{
    int firstFrame = 2;

    HRESULT res;
    D3D11_MAPPED_SUBRESOURCE texmap;
    res = d3d_ctx->Map(video_tex[1 - video_index].texture, 0, D3D11_MAP_WRITE_DISCARD, 0, &texmap);
    assert(SUCCEEDED(res));
    video_data = (uint8_t*)texmap.pData;

    while (running)
    {
        auto frame = file.videoStreams[0].GetFrame(timer.Get());
        if (firstFrame <= 0)
        {
            if (!Sync(frame))
                break;
        }
        else
        {
            timer.Set((float)frame.pts);
            firstFrame--;
        }

        //printf("BasePlayer::VideoThread - render frame time: %fs delay: %fs\r", frame.pts, timer.Get() - frame.pts);
        {
            //std::lock_guard<std::mutex> lock(mutex); //todo: use another mutex?
            //vframes.push_front(std::move(frame));
            //last_vframe = std::move(frame);
            if (!video_ready && video_data)
            {
                memcpy(video_data, frame.data, frame.size);
                video_ready = true;
            }
        }

        //// Upload decoded frame to texture data
        //glBindTexture(GL_TEXTURE_2D, video_tex);
        //glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, video_w, video_h + video_h / 2, GL_LUMINANCE, GL_UNSIGNED_BYTE, frame.data);

    }
    printf("BasePlayer::VideoThread - END\n");
}

void BasePlayer::AudioThread()
{
    ALint val;
    while (file.has_audio && running)
    {
        auto frame = file.audioStreams[0].GetFrame();
        if (!Sync(frame))
            break;
        //printf("   BasePlayer::AudioThread - render frame\n");

        ALuint albuffer;
        alGenBuffers(1, &albuffer);
        alBufferData(albuffer, AL_FORMAT_STEREO16, frame.data, frame.size, frame.sampleRate);
        alSourceQueueBuffers(audio_source, 1, &albuffer);
        audio_buffer_queue.push_front(albuffer);
        CleanAudioBuffer();
        alGetSourcei(audio_source, AL_SOURCE_STATE, &val);
        if (playing && val != AL_PLAYING)
            alSourcePlay(audio_source);
    }
    printf("BasePlayer::AudioThread - END\n");
}

void BasePlayer::CleanAudioBuffer()
{
    ALint val;
    // Remove consumed buffers
    alGetSourcei(audio_source, AL_BUFFERS_PROCESSED, &val);
    while (val--)
    {
        alSourceUnqueueBuffers(audio_source, 1, &audio_buffer_queue.back());
        alDeleteBuffers(1, &audio_buffer_queue.back());
        audio_buffer_queue.pop_back();
    }
}

bool BasePlayer::Init(ID3D11Device* device, ID3D11DeviceContext* ctx)
{
    d3d_device = device;
    d3d_ctx = ctx;
    av_register_all();
    avformat_network_init();
    oalDevice = alcOpenDevice(nullptr);
    oalContext = alcCreateContext(oalDevice, nullptr);
    alcMakeContextCurrent(oalContext);
    alGenSources(1, &audio_source);
    return true;
}

void BasePlayer::Destroy()
{
    if (audio_source)
    {
        CleanAudioBuffer();
        alDeleteSources(1, &audio_source);
    }
    alcMakeContextCurrent(nullptr);
    alcDestroyContext(oalContext);
    alcCloseDevice(oalDevice);
}

bool BasePlayer::Open(const std::string filename)
{
    if (!file.Open(filename.c_str()))
        return false;
    video_w = file.videoStreams[0].width;
    video_h = file.videoStreams[0].height;
    video_tex[0].Create(d3d_device, video_w, video_h + video_h / 2,
        D3D11_BIND_SHADER_RESOURCE, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE, DXGI_FORMAT_R8_UNORM, 1);
    video_tex[1].Create(d3d_device, video_w, video_h + video_h / 2,
        D3D11_BIND_SHADER_RESOURCE, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE, DXGI_FORMAT_R8_UNORM, 1);
    running = true;
    vthread = std::thread(&This::VideoThread, this);
    athread = std::thread(&This::AudioThread, this);
    return true;
}

void BasePlayer::Start()
{
    alSourcePlay(audio_source);
    timer.Start();
    playing = true;
    play_cv.notify_all();
}

void BasePlayer::Pause()
{
    alSourcePause(audio_source);
    timer.Stop();
    playing = false;
}

void BasePlayer::Close()
{
    alSourceStop(audio_source);
    running = false;
    playing = false;
    timer.Stop();
    file.Stop();
    play_cv.notify_all();
    if (vthread.joinable()) vthread.join();
    if (athread.joinable()) athread.join();
}

void BasePlayer::Toggle()
{
    printf("BasePlayer::Toggle\n");
    playing ? Pause() : Start();
}

bool BasePlayer::UpdateTexture()
{
    //std::lock_guard<std::mutex> lock(mutex);
    if (video_ready)
    {
        video_index = 1 - video_index;

        d3d_ctx->Unmap(video_tex[video_index].texture, 0);

        HRESULT res;
        D3D11_MAPPED_SUBRESOURCE texmap;
        res = d3d_ctx->Map(video_tex[1 - video_index].texture, 0, D3D11_MAP_WRITE_DISCARD, 0, &texmap);
        assert(SUCCEEDED(res));
        video_data = (uint8_t*)texmap.pData;

        video_ready = false;
    }
    return true;
}

const Texture2D& BasePlayer::GetTexture() const
{
    //std::lock_guard<std::mutex> lock(mutex);
    return video_tex[video_index];
}
