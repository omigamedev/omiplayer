#include "stdafx.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb.h"
#include "stb_image.h"

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "libovr.lib")
#pragma comment(lib, "openvr_api.lib")
#pragma comment(lib, "OpenAL32.lib")
#pragma comment(lib, "avcodec.lib")
#pragma comment(lib, "avdevice.lib")
#pragma comment(lib, "avfilter.lib")
#pragma comment(lib, "avformat.lib")
//#pragma comment(lib, "avresample.lib")
#pragma comment(lib, "avutil.lib")
#pragma comment(lib, "swscale.lib")
//#pragma comment(lib, "ovraudio.link32.lib")
#pragma comment(lib, "shlwapi.lib")
