#pragma once

namespace omi
{
	struct Adapter
	{
		IDXGIAdapter* dx_adapter;
		LUID luid;
		int memory;
		char description[128];

		Adapter(IDXGIAdapter* adapter);
		Adapter(const Adapter& other) = delete;
		Adapter(Adapter&& other);
		~Adapter();

		bool operator==(const Adapter& other) const;
		bool operator==(const LUID& otherLuid) const;

	};

	struct Texture2D
	{
		ID3D11Texture2D* texture;
		ID3D11ShaderResourceView* view;
		int w, h;

		bool Create(ID3D11Device* device, int width, int height, D3D11_BIND_FLAG bind, D3D11_USAGE usage, 
			int access, DXGI_FORMAT format, int mip_levels, void* data = nullptr);
		bool Create(ID3D11Device* device, ID3D11Texture2D* tex, bool createShaderResource = false);
		void Update(ID3D11DeviceContext* ctx, void* data);
	};

	struct DepthBuffer
	{
		ID3D11Texture2D* texture;
		ID3D11DepthStencilView* view;
		ID3D11DepthStencilState* state;

		void Create(ID3D11Device* device, int width, int height);
	};

	struct RenderTarget
	{
		Texture2D color;
		DepthBuffer depth;
		ID3D11RenderTargetView* view;
		ID3D11RasterizerState* rasterizer;
		int w, h;

		bool Create(ID3D11Device* device, int width, int height);
		bool Create(ID3D11Device* device, ID3D11Texture2D* tex);
		void Clear(ID3D11DeviceContext* ctx, float r, float g, float b, float a = 1.f);
		void Use(ID3D11DeviceContext* ctx);
	};

	struct MultiRenderTarget
	{
		struct Target
		{
			Texture2D color;
			ID3D11RasterizerState* rasterizer;
			ID3D11RenderTargetView* view;
		};

		DepthBuffer depth;
		std::vector<Target> targets;
		ID3D11RasterizerState* rasterizer;
		int w, h, currentIndex{ 0 };

		bool Create(ID3D11Device* device, int width, int height);
		bool AddTarget(ID3D11Device* device, ID3D11Texture2D* tex = nullptr);
		void Clear(ID3D11DeviceContext* ctx, float r, float g, float b, float a = 1.f);
		void Use(ID3D11DeviceContext* ctx);
	};

	struct DataBuffer
	{
		int size;
		ID3D11Buffer* d3d_buffer{ nullptr };

		DataBuffer() = default;
		DataBuffer(const DataBuffer&) = delete;
		DataBuffer(DataBuffer&& other);

		bool Create(ID3D11Device* d3d_device, D3D11_USAGE usage, D3D11_BIND_FLAG bind, int access, int size, const void* raw_data = nullptr);

		template<typename T>
		bool Create(ID3D11Device* d3d_device, D3D11_USAGE usage, D3D11_BIND_FLAG bind, int access, const std::vector<T> &raw_data)
		{
			return Create(d3d_device, usage, bind, access, raw_data.size() * sizeof(T), raw_data.data());
		}

		template<typename T, std::size_t size>
		bool Create(ID3D11Device* d3d_device, D3D11_USAGE usage, D3D11_BIND_FLAG bind, int access, const T(&raw_data)[size])
		{
			return Create(d3d_device, usage, bind, access, size * sizeof(T), raw_data);
		}

		template<typename T>
		bool Create(ID3D11Device* d3d_device, D3D11_USAGE usage, D3D11_BIND_FLAG bind, int access)
		{
			return Create(d3d_device, usage, bind, access, sizeof(T));
		}

		~DataBuffer();
	};

	template<typename DataType, typename VertexType>
	struct Shader
	{
		typedef DataType Data;
		typedef struct Data::PerApp PerAppType;
		typedef struct Data::PerFrame PerFrameType;

		ID3D11PixelShader*  d3d_pixelShader{ nullptr };
		ID3D11VertexShader* d3d_vertexShader{ nullptr };
		ID3D11InputLayout*  d3d_vertexLayout{ nullptr };
		DataBuffer paBuffer;
		DataBuffer pfBuffer;

		bool Create(ID3D11Device* device, const void* vertexBytecode, int vertexSize, const void* pixelBytecode, int pixelSize)
		{
			HRESULT res;
			res = device->CreatePixelShader(pixelBytecode, pixelSize, nullptr, &d3d_pixelShader);
			assert(SUCCEEDED(res));
			res = device->CreateVertexShader(vertexBytecode, vertexSize, nullptr, &d3d_vertexShader);
			assert(SUCCEEDED(res));
			res = device->CreateInputLayout(VertexType::Layout, _countof(VertexType::Layout),
				vertexBytecode, vertexSize, &d3d_vertexLayout);
			assert(SUCCEEDED(res));
			paBuffer.Create<PerAppType>(device, D3D11_USAGE_DEFAULT, D3D11_BIND_CONSTANT_BUFFER, 0);
			pfBuffer.Create<PerFrameType>(device, D3D11_USAGE_DEFAULT, D3D11_BIND_CONSTANT_BUFFER, 0);
			return true;
		}

		template<typename T, std::size_t vSize, std::size_t pSize>
		bool Create(ID3D11Device* device, const T(&vertexBytecode)[vSize], const T(&pixelBytecode)[pSize])
		{
			return Create(device, vertexBytecode, vSize * sizeof(T), pixelBytecode, pSize * sizeof(T));
		}

		void UpdatePerFrameData(ID3D11DeviceContext* ctx, const PerFrameType& data)
		{
			ctx->UpdateSubresource(pfBuffer.d3d_buffer, 0, nullptr, &data, 0, 0);
		}

		void UpdatePerAppData(ID3D11DeviceContext* ctx, const PerAppType& data)
		{
			ctx->UpdateSubresource(paBuffer.d3d_buffer, 0, nullptr, &data, 0, 0);
		}

		void Use(ID3D11DeviceContext* ctx)
		{
			ctx->IASetInputLayout(d3d_vertexLayout);
			ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			ctx->PSSetShader(d3d_pixelShader, nullptr, 0);
			ctx->VSSetShader(d3d_vertexShader, nullptr, 0);
			ctx->VSSetConstantBuffers(0, 1, &paBuffer.d3d_buffer);
			ctx->VSSetConstantBuffers(1, 1, &pfBuffer.d3d_buffer);
		}
	};

	class DX
	{
	public:
		UINT bufferW, bufferH;
		ID3D11Device*             d3d_device{ nullptr };
		ID3D11DeviceContext*      d3d_context{ nullptr };
		IDXGISwapChain*           dx_swapchain{ nullptr };
		mutable RenderTarget framebuffer;

	public:
		static std::vector<Adapter> EnumerateAdapters();

		ID3D11Device* GetDevice();
		ID3D11DeviceContext* GetContext();

		void CreateDeviceAndSwapChain(const Adapter& adapter, const HWND handle, UINT bufferWidth, UINT bufferHeight);
		void CreateFramebuffer();
		bool Create(const Adapter& adapter, const HWND handle, int width, int height);
		void ClearRenderTargetView(float r, float g, float b, float a = 1.f) const;
        inline void UseFramebuffer() const { framebuffer.Use(d3d_context); }
		void Present() const;
	};

}

