#pragma once

#include "Window.h"
#include "DX.h"
#include "HMD.h"

namespace omi
{
	struct OculusFramebuffer : public MultiRenderTarget
	{
        ovrTextureSwapChain TextureSet;
		ovrRecti eyeRenderViewport;
		ovrSizei idealSize;

		bool Create(ID3D11Device* device, ovrSession session, int width, int height);
		void SetViewport(ID3D11DeviceContext* ctx);
		void Commit(ovrSession session);
	};

	struct OculusRift : public HMDInterface
	{
		OculusFramebuffer eyes[2];
		ovrSession session;
		ovrHmdDesc hmdDesc;
		ovrGraphicsLuid luid;
		ovrMirrorTexture mirrorTexture;
		ovrPosef EyeRenderPose[2];
		ovrEyeRenderDesc eyeRenderDesc[2];
		ovrVector3f HmdToEyeViewOffset[2];
		bool visible{ true };

		virtual bool Initialize() override;
		virtual bool Create(const DX& dx, const Window& window) override;
		virtual void Draw(const DX& dx, DrawFunction lambda, PostFunction postRender) override;
        virtual void Recenter() override;
        virtual int GetEyeWidth() override { return eyes[0].idealSize.w; }
        virtual int GetEyeHeight() override { return eyes[0].idealSize.h; }
		
		auto GetPose() { return EyeRenderPose[0]; }

        virtual Adapter GetAdapter(const DX& dx) override;

    };
}
