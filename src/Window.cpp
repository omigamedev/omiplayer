#include "stdafx.h"
#include "Window.h"

using namespace omi;

std::map<HWND, Window*> Window::_registeredWindows;

LRESULT CALLBACK Window::_MessageHandler(HWND hWnd, UINT msgID, WPARAM wp, LPARAM lp)
{
	if (msgID == WM_CREATE)
	{
		CREATESTRUCT* cs = reinterpret_cast<CREATESTRUCT*>(lp);
		_registeredWindows[hWnd] = reinterpret_cast<Window*>(cs->lpCreateParams);
	}
	
	auto w = _registeredWindows.find(hWnd);
	if (w == _registeredWindows.end())
		return DefWindowProc(hWnd, msgID, wp, lp);
	
	return w->second->HandleMessage(hWnd, msgID, wp, lp);
}

LRESULT Window::HandleMessage(HWND hWnd, UINT msgID, WPARAM wp, LPARAM lp)
{
	switch (msgID)
	{
	case WM_CREATE:
		return 0;
	case WM_KEYDOWN: // TODO: remove this
		if (keyDown) keyDown(wp);
		break;
	case WM_CLOSE:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hWnd, msgID, wp, lp);
}

bool Window::Create(int width, int height)
{
	static char title[256];
	w = width;
	h = height;

	WNDCLASS wc{ 0 };
	wc.hInstance = GetModuleHandle(nullptr);
	wc.lpfnWndProc = _MessageHandler;
	wc.lpszClassName = "omiWindowClass";
	wc.style = CS_HREDRAW | CS_VREDRAW;
	RegisterClass(&wc);

	RECT adjustedRect{ 0 };
	adjustedRect.bottom = height;
	adjustedRect.right = width;
	AdjustWindowRect(&adjustedRect, WS_OVERLAPPEDWINDOW, FALSE);
	handle = CreateWindowEx(0, wc.lpszClassName, title, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, adjustedRect.right - adjustedRect.left,
		adjustedRect.bottom - adjustedRect.top,
		NULL, NULL, GetModuleHandle(nullptr), this);

	if (!handle)
		return false;

	ShowWindow(handle, SW_NORMAL);

	return true;
}

Window::Window() : w(0), h(0)
{

}

Window::~Window()
{

}
